# Keys

## Certificate
openssl req -x509 -new -nodes -keyout key.pem -out certificate.pem

## Key
openssl pkcs8 -nocrypt -in key.pem -out key.pk8 -outform der -topk8