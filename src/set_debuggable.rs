use std::{fs, path::PathBuf};

use crate::Runnable;
use anyhow::{Context, Result};
use clap::Parser;

#[derive(Parser)]
pub struct SetDebuggable {
    pub application_dir: PathBuf,
}

const FROM: &'static str = "<application ";
const TO: &'static str = r#"<application android:debuggable="true" "#;

impl Runnable for SetDebuggable {
    fn name(&self) -> &'static str {
        "Set APK debuggable"
    }
    fn run(&mut self) -> Result<bool> {
        let manifest_path = self.application_dir.join("AndroidManifest.xml");

        let manifest = fs::read_to_string(&manifest_path).context("Manifest read error")?;
        let dbg_manifest = manifest.replace(FROM, TO);

        if manifest.len() == dbg_manifest.len() {
            return Ok(false);
        }

        fs::write(&manifest_path, dbg_manifest.as_bytes()).context("Can't write to manifest")?;
        Ok(true)
    }
}
