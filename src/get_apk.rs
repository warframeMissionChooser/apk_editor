use crate::Runnable;
use anyhow::{Context, Result};
use clap::Parser;
use indicatif::{HumanBytes, ProgressBar, ProgressStyle};
use regex::Regex;
use reqwest::{
    blocking::Client,
    header::{IF_MODIFIED_SINCE, LAST_MODIFIED},
    Url,
};
use std::{
    fs::{metadata, File},
    io,
    path::PathBuf,
};

/// Download the lastest APK from the web
#[derive(Parser)]
pub struct GetApk {
    /// Url to downlaod the apk_url
    #[clap(long, default_value = SOURCE_URL)]
    pub url: Url,

    /// Url to downlaod from
    #[clap(long)]
    pub apk_url: Option<Url>,

    /// User Agent used while downloading HTML pages and APK
    #[clap(long, default_value = APP_USER_AGENT)]
    pub user_agent: String,

    /// Apk file
    #[clap(long, default_value = "warframe.apk")]
    pub file_name: PathBuf,
}

impl Default for GetApk {
    fn default() -> Self {
        Self {
            url: Url::parse(SOURCE_URL).unwrap(),
            apk_url: None,
            user_agent: APP_USER_AGENT.to_owned(),
            file_name: "warframe.apk".into(),
        }
    }
}

static SOURCE_URL: &str = "https://apkpure.com/warframe/com.digitalextremes.warframenexus/download";
static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

impl GetApk {
    fn get_url(&self, client: &Client) -> Result<Url> {
        let html = client.get(self.url.clone()).send()?.text()?;

        let r = Regex::new(r#""[\w:/.]*/APK/[^"]*""#).unwrap();
        let url = r.find(&html).context("No url found in webpage")?;

        let url = url.as_str().trim_matches('"');
        let url = Url::parse(url).context("Failed to parse url")?;

        Ok(url)
    }

    fn download(&self, client: &Client) -> Result<bool> {
        use chrono::{offset::Utc, DateTime};
        let date: Option<DateTime<Utc>> = metadata(&self.file_name)
            .ok()
            .and_then(|m| m.modified().ok())
            .map(|d| d.into());

        let apk = client.get(
            self.apk_url
                .clone()
                .context("Download called, but no APK url set")?,
        );
        println!("Download from {}", self.apk_url.as_ref().unwrap());

        let apk = if let Some(date) = date {
            apk.header(IF_MODIFIED_SINCE, date.to_rfc2822())
        } else {
            apk
        }
        .send()
        .context("Failed to download APK")?;

        let last_modified = apk
            .headers()
            .get(LAST_MODIFIED)
            .and_then(|e| e.to_str().ok())
            .and_then(|e| DateTime::parse_from_rfc2822(e).ok());

        match (date, last_modified) {
            (Some(date), Some(last)) if date >= last => {
                println!("Current version (downloaded on {}) is more recent than the version reported by the server {}",
                    date,
                    last,
                );
                return Ok(false);
            }
            _ => (),
        }

        let mut bar = {
            let style = ProgressStyle::default_bar()
                .template("{wide_bar} {eta} left. {bytes} / {total_bytes} @ {bytes_per_sec}")
                .unwrap();

            let size = apk.content_length().expect("Remote did not report a size");
            ProgressBar::new(size).with_style(style)
        }
        .wrap_read(apk);

        let mut file = File::create(&self.file_name).context("Failed to create APK file")?;
        let size = io::copy(&mut bar, &mut file).context("Failed while download APK to file")?;

        drop(bar);
        drop(file);

        println!(
            "APK ({}) downloaded to {}",
            HumanBytes(size),
            self.file_name.display()
        );

        Ok(true)
    }
}

impl Runnable for GetApk {
    fn name(&self) -> &'static str {
        "Download APK from Web"
    }

    fn run(&mut self) -> Result<bool> {
        let client = Client::builder()
            .user_agent(&self.user_agent)
            .build()
            .context("Failed to Build HTTP client")?;

        match self.apk_url {
            Some(_) => (),
            None => self.apk_url = Some(self.get_url(&client).context("Failed to get APK url")?),
        };

        self.download(&client).context("Unable to download APK")
    }
}
