use std::{
    io::{BufRead, BufReader, Write},
    process::{Command, Stdio},
};

use anyhow::{bail, Result};

use crate::Runnable;

type CommandBuilder = Result<Option<(Command, Option<usize>)>>;

pub fn command_with_line(cmd: Command, line: usize) -> CommandBuilder {
    command_builder(cmd, Some(line))
}

pub fn command_no_line(cmd: Command) -> CommandBuilder {
    command_builder(cmd, None)
}

fn command_builder(cmd: Command, nb_line: Option<usize>) -> CommandBuilder {
    Ok(Some((cmd, nb_line)))
}

pub trait RunnableCommand {
    fn name(&self) -> &'static str;
    fn create_command(&mut self) -> Result<Option<(Command, Option<usize>)>>;
}

impl<T: RunnableCommand> Runnable for T {
    fn name(&self) -> &'static str {
        T::name(&self)
    }

    fn run(&mut self) -> Result<bool> {
        let cmd = self.create_command()?;

        let (mut cmd, nb_line) = if let Some(cmd) = cmd {
            cmd
        } else {
            return Ok(false);
        };

        let nb_line = nb_line.map(|e| e.to_string()).unwrap_or("??".to_owned());
        let mut hndl = cmd.stdout(Stdio::piped()).spawn()?;

        if let Some(out) = hndl.stdout.as_mut() {
            let out = BufReader::new(out);
            for (id, _) in out.lines().enumerate() {
                print!(
                    "\r{:>25}: {}",
                    format!("[{}/{}]", id + 1, nb_line),
                    self.name(),
                );

                std::io::stdout().flush()?;
            }
            println!()
        } else {
            println!("{:27}Unable to get progress ...", "");
        }

        if !hndl.wait()?.success() {
            bail!("Command returned an error, see logs for more details");
        }

        Ok(true)
    }
}
