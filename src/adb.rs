use std::{
    path::PathBuf,
    process::{Command, Stdio},
};

use anyhow::{bail, Result};
use clap::Parser;

use crate::{
    command_runner::{command_no_line, RunnableCommand},
    Runnable,
};

fn adb() -> Command {
    Command::new("adb")
}

#[derive(Parser)]
pub struct WaitDevice;

impl RunnableCommand for WaitDevice {
    fn name(&self) -> &'static str {
        "Wait for device"
    }

    fn create_command(&mut self) -> Result<Option<(Command, Option<usize>)>> {
        let mut cmd = adb();
        cmd.arg("wait-for-device");

        command_no_line(cmd)
    }
}

#[derive(Parser)]
pub struct Install {
    pub apk: PathBuf,
}

impl RunnableCommand for Install {
    fn name(&self) -> &'static str {
        "Installing APK"
    }

    fn create_command(&mut self) -> Result<Option<(Command, Option<usize>)>> {
        let mut cmd = adb();
        cmd.arg("install-multiple").arg(&self.apk)/*.args([
            "./tmp/config.fr.SIGN.apk",
            "./tmp/config.arm64_v8a.SIGN.apk",
            "./tmp/config.xhdpi.SIGN.apk",
        ])*/;

        command_no_line(cmd)
    }
}

#[derive(Parser)]
pub struct RunDebug {
    pub apk: String,
}

impl RunnableCommand for RunDebug {
    fn name(&self) -> &'static str {
        "Running APK with debugger"
    }

    fn create_command(&mut self) -> Result<Option<(Command, Option<usize>)>> {
        let mut cmd = adb();
        cmd.arg("shell")
            .arg("am")
            .arg("start")
            .arg("-D")
            .arg(&self.apk);

        command_no_line(cmd)
    }
}

#[derive(Parser, Default)]
pub struct JdwpForward {
    pub port: Option<u32>,
}

impl Runnable for JdwpForward {
    fn name(&self) -> &'static str {
        "Forwarding JDWP port locally"
    }

    fn run(&mut self) -> Result<bool> {
        let pid = loop {
            if let Some(pid) = jdwp()? {
                break pid;
            }

            std::thread::sleep(std::time::Duration::from_secs(1));
        };

        println!("Android process: {}", pid);

        let port = match forward(&format!("jdwp:{}", pid))? {
            Some(port) => port,
            None => {
                bail!("Unable to forward jdwp:{} locally", pid);
            }
        };

        println!("Exposing debugger locally on port {}", port);
        self.port = Some(port);

        Ok(true)
    }
}

fn jdwp() -> Result<Option<u32>> {
    let mut child = Command::new("adb")
        .arg("jdwp")
        .stdout(Stdio::piped())
        .spawn()?;

    std::thread::sleep(std::time::Duration::from_millis(100));
    child.kill()?;
    let output = child.wait_with_output()?;

    let pid = String::from_utf8(output.stdout)?.trim().parse().ok();

    Ok(pid)
}

pub fn forward(port: &str) -> Result<Option<u32>> {
    let out = Command::new("adb")
        .arg("forward")
        .arg("tcp:0")
        .arg(port)
        .output()?;

    let port = String::from_utf8(out.stdout)?.trim().parse().ok();
    Ok(port)
}
