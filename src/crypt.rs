use std::path::PathBuf;

use aes::Aes128;
use anyhow::Result;
use block_modes::{block_padding::Pkcs7, BlockMode, Cbc};
use walkdir::WalkDir;

use crate::Runnable;

type Aes128Cbc = Cbc<Aes128, Pkcs7>;

struct Cypher(Aes128Cbc);

impl Cypher {
    pub fn from(key: &[u8], iv: &[u8]) -> Self {
        Self(Aes128Cbc::new_var(&key, iv).unwrap())
    }

    pub fn decrypt<'a>(&self, mut buffer: Vec<u8>) -> String {
        let dec = self.0.clone();

        let res = dec.decrypt(&mut buffer).unwrap();
        let len = res.len();
        buffer.truncate(len);
        String::from_utf8(buffer).unwrap()
    }

    pub fn crypt<'a>(&self, mut buffer: Vec<u8>) -> Vec<u8> {
        let dec = self.0.clone();

        // Extend buffer so padding will fit
        let pos = buffer.len();
        buffer.resize(pos + 16, 0);

        // Encrypt
        let res = dec.encrypt(&mut buffer, pos).unwrap();
        let len = res.len();

        // Resize buffer to right size
        buffer.truncate(len);
        buffer
    }
}

#[derive(Clone)]
pub struct KeyInfo {
    pub key: Vec<u8>,

    pub iv: Vec<u8>,
}

impl KeyInfo {
    fn to_cypher(&self) -> Cypher {
        Cypher::from(&self.key, &self.iv)
    }
}

pub struct DecryptFile {
    pub key_info: KeyInfo,

    /// Path to use to decrypt files
    /// If it's a folder recursly decrypt files
    pub path: PathBuf,
}

impl Runnable for DecryptFile {
    fn name(&self) -> &'static str {
        "Decrypt file"
    }

    fn run(&mut self) -> Result<bool> {
        let cypher = self.key_info.to_cypher();

        let mut changed = false;
        for f in WalkDir::new(&self.path) {
            let f = f.unwrap();

            let (dec, enc) = match get_file_pair(f.into_path()) {
                Some(v) => v,
                None => continue,
            };

            let raw_enc_data = std::fs::read(&enc).unwrap();
            let dec_data = std::fs::read(&dec);

            let decoded = cypher.decrypt(raw_enc_data);

            if dec_data.map_or(false, |d| d == decoded.as_bytes()) {
                println!("{:27}Unchanged file : {:?}", "", &enc);
                continue;
            };

            println!("{:27}Decrypted file : {:?}", "", &enc);

            changed = true;
            std::fs::write(&dec, decoded)?;
        }

        Ok(changed)
    }
}

fn get_file_pair(path: PathBuf) -> Option<(PathBuf, PathBuf)> {
    let valid_ext = path.extension().map_or(false, |ext| ext == "bin");
    if !valid_ext {
        return None;
    }

    valid_ext.then(move || (path.with_extension(""), path))
}

pub struct EncryptFile {
    pub key_info: KeyInfo,

    /// Path to use to encrypt files
    /// If it's a folder recursly encrypt files
    pub path: PathBuf,
}

impl Runnable for EncryptFile {
    fn name(&self) -> &'static str {
        "Encrypt file"
    }

    fn run(&mut self) -> Result<bool> {
        let cypher = self.key_info.to_cypher();

        for f in WalkDir::new(&self.path) {
            let f = f.unwrap();

            let (dec, enc) = match get_file_pair(f.into_path()) {
                Some(v) => v,
                None => continue,
            };

            println!("{:27}Start to encrypt file : {:?}", "", dec);

            let raw_data = std::fs::read(&dec).unwrap();

            let encoded = cypher.crypt(raw_data);

            std::fs::write(&enc, encoded)?;
        }

        Ok(true)
    }
}
