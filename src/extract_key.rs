use crate::{
    adb::{JdwpForward, RunDebug},
    Runnable,
};

use anyhow::{bail, Context, Result};
use clap::Parser;

type Key = Vec<u8>;

#[derive(Parser, Debug, Default)]
pub struct ExtractKey {
    #[clap(skip)]
    pub key: Option<Key>,

    #[clap(skip)]
    pub iv: Option<Key>,
}

impl Runnable for ExtractKey {
    fn name(&self) -> &'static str {
        "Extract Key from running APK"
    }

    fn run(&mut self) -> Result<bool> {
        if self.key.is_some() && self.iv.is_some() {
            return Ok(false);
        }

        RunDebug {
            apk: "com.digitalextremes.warframenexus/.WarframeCompanionActivity".to_owned(),
        }
        .do_run(true)?;

        println!("Application started on mobile, wait for the debugger to listen for incomming conenction");

        let mut jdwp = JdwpForward::default();
        jdwp.do_run(true)?;
        let port = jdwp.port.context("JDWP returned no port")?;

        let mut jdb = Jdb::connect(port)?;
        jdb.wait_init()?;

        jdb.run("stop in javax.crypto.Cipher.init(int, java.security.Key, java.security.AlgorithmParameters)")?;
        jdb.run("stop in javax.crypto.Cipher.init(int, java.security.Key, java.security.spec.AlgorithmParameterSpec)")?;
        jdb.run("monitor dump key.key")?;
        jdb.run("monitor dump params.iv")?;

        let mut key = None;
        let mut iv = None;
        while key.is_none() || iv.is_none() {
            let line = jdb.next_line()?;

            if line.contains("params.iv") {
                iv = Some(jdb.next_line()?);
            }

            if line.contains("key") {
                key = Some(jdb.next_line()?);
            }
        }

        if !jdb.quit()? {
            bail!("Error while running / exiting JDB");
        }

        let key = key.unwrap();
        let iv = iv.unwrap();

        fn parse_array(str: String) -> Vec<u8> {
            fn to_u8(from: i8) -> u8 {
                from as u8
            }

            str.split(',')
                .map(str::trim)
                .map(str::parse::<i8>)
                .map(Result::unwrap)
                .map(to_u8)
                .collect()
        }

        self.key = Some(parse_array(key));
        self.iv = Some(parse_array(iv));

        println!("infos: {:?} \t {:?}", self.key, self.iv);

        Ok(true)
    }
}

use jdb::Jdb;
mod jdb {
    use anyhow::{Context, Result};
    use std::{
        io::{BufRead, BufReader, Lines, Write},
        process::{Child, ChildStdin, ChildStdout, Command, Stdio},
    };

    pub struct Jdb {
        process: Child,
        stdin: ChildStdin,
        stdout: Lines<BufReader<ChildStdout>>,
    }

    impl Jdb {
        pub fn connect(port: u32) -> Result<Self> {
            let mut c = Command::new("jdb");
            c.arg("-connect")
                .arg(format!(
                    "com.sun.jdi.SocketAttach:hostname=localhost,port={}",
                    port
                ))
                .stdout(Stdio::piped())
                .stdin(Stdio::piped());

            let mut process = c.spawn()?;

            let stdin = process.stdin.take().unwrap();
            let stdout = BufReader::new(process.stdout.take().unwrap()).lines();

            Ok(Self {
                process,
                stdin,
                stdout,
            })
        }

        pub fn next_line(&mut self) -> Result<String> {
            Ok(self.stdout.next().context("JDB out is closed")??)
        }

        pub fn wait_init(&mut self) -> Result<()> {
            loop {
                let line = self.next_line()?;

                if line.trim() == "Initializing jdb ..." {
                    break;
                }
            }

            Ok(())
        }

        pub fn run(&mut self, msg: &str) -> Result<()> {
            self.stdin.write_all(msg.as_bytes())?;
            self.stdin.write_all(b"\n")?;

            Ok(())
        }

        pub fn quit(mut self) -> Result<bool> {
            self.run("quit")?;
            let status = self.process.wait()?.success();
            Ok(status)
        }
    }
}
