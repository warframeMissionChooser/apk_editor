use std::{path::PathBuf, process::Command};

use crate::command_runner::{command_no_line, command_with_line, RunnableCommand};
use anyhow::Result;
use clap::Parser;

pub trait ApkToolCommand {
    fn name(&self) -> &'static str;
    fn create_command(&mut self, cmd: &mut Command) -> usize;
}

impl<T: ApkToolCommand> RunnableCommand for T {
    fn name(&self) -> &'static str {
        T::name(&self)
    }

    fn create_command(&mut self) -> Result<Option<(Command, Option<usize>)>> {
        let mut cmd = Command::new("java");
        let nb_line = self.create_command(cmd.arg("-jar").arg("jar/apktool.jar"));

        command_with_line(cmd, nb_line)
    }
}

/// Decode an APK into a folder
#[derive(Parser)]
pub struct DecodeApk {
    pub apk_file: PathBuf,
    pub target: PathBuf,
}

impl ApkToolCommand for DecodeApk {
    fn name(&self) -> &'static str {
        "Decode APK to folder"
    }

    fn create_command(&mut self, cmd: &mut Command) -> usize {
        cmd.arg("decode")
            .arg(&self.apk_file)
            .arg("-f")
            .arg("--no-src")
            .arg("-o")
            .arg(&self.target);

        12
    }
}

#[derive(Parser)]
pub struct BuildApk {
    pub src: PathBuf,
    pub target: PathBuf,
}

impl ApkToolCommand for BuildApk {
    fn name(&self) -> &'static str {
        "Build APK"
    }

    fn create_command(&mut self, cmd: &mut Command) -> usize {
        cmd.arg("build").arg(&self.src).arg("-o").arg(&self.target);

        10
    }
}

#[derive(Parser)]
pub struct SignApk {
    pub src: PathBuf,
    pub target: PathBuf,
}

impl RunnableCommand for SignApk {
    fn name(&self) -> &'static str {
        "Sign APK"
    }

    fn create_command(&mut self) -> Result<Option<(Command, Option<usize>)>> {
        let mut cmd = Command::new("java");
        cmd.arg("-jar")
            .arg("jar/signapk.jar")
            .arg("keys/cert.pem")
            .arg("keys/key.pk8")
            .arg(&self.src)
            .arg(&self.target);

        command_no_line(cmd)
    }
}
